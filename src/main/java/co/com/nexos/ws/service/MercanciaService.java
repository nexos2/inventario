package co.com.nexos.ws.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import co.com.nexos.ws.dto.MercanciaDTO;
import co.com.nexos.ws.entities.Mercancia;
import co.com.nexos.ws.entities.Usuario;
import co.com.nexos.ws.repository.MercanciaRepository;
import co.com.nexos.ws.repository.UsuarioRepository;
import co.com.nexos.ws.ui.model.response.Response;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Service
public class MercanciaService {
	
	@Autowired
	MercanciaRepository mercanciaRepository; 
	@Autowired
	UsuarioRepository usuarioRepository; 
	
	private Logger log = Logger.getLogger("Log mercancia");
	
	public Response createMaercancia(MercanciaDTO mercanciaDTO) {
		log.log(Level.INFO, "Inicio Guardado mercancia");
		Usuario usuarioFind = usuarioRepository.findByUsuarioId(mercanciaDTO.getUsuarioIdCreate());
		if (usuarioFind != null) {
			Mercancia mercancia = new Mercancia();
			mercancia.setIdMercancia(mercanciaDTO.getIdMercancia());
			mercancia.setCantidad(mercanciaDTO.getCantidad());
			mercancia.setFechaIngreso(mercanciaDTO.getFechaIngresa());
			mercancia.setNombreMercancia(mercanciaDTO.getNombre());
			mercancia.setUsuarioIdCreate(mercanciaDTO.getUsuarioIdCreate());
			mercanciaRepository.save(mercancia);
			log.log(Level.INFO, "Termina el metodo");
			return new Response(HttpStatus.OK.value(), "Registro Exitoso", mercancia);
		} else {
			log.log(Level.INFO, "Error al guardar en sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro usuario en el sistema", "{}");
		}
	}
	
	public Response findMercanciaByIdMercancia(long idMercancia) {		
		log.log(Level.INFO, "Inicio busqueda de mercancia");
		Mercancia mercanciaFind = mercanciaRepository.findByIdMercancia(idMercancia);
		if(mercanciaFind!=null) {
			log.log(Level.INFO, "Termina el metodo");
			return new Response(HttpStatus.OK.value(), "Busqueda Exitosa",mercanciaFind);
		}else {
			log.log(Level.INFO, "No existe mercancia en el sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro mercancia en el sistema","{}");
		}		
	}
	
	public List<Mercancia> getAll() {
		log.log(Level.INFO, "Inicio listado de mercancia");		
		List<Mercancia> listaUsuarios = mercanciaRepository.findAll();
		log.log(Level.INFO, "Termina el metodo");
		return listaUsuarios;
	}
	
	public Response updateMercancia(long idMercancia, int cantidad, Date fechaUpdate, String nombreMercancia, String usuarioIdUpdate) {

		log.log(Level.INFO, "Inicio Actualizacion de mercancia");		
		Usuario usuarioUpdate = usuarioRepository.findByUsuarioId(usuarioIdUpdate);
		if(usuarioUpdate !=null) {
			Mercancia mercanciaFind = mercanciaRepository.findByIdMercancia(idMercancia);
			if(mercanciaFind != null) {				
				mercanciaFind.setCantidad(cantidad);
				mercanciaFind.setFechaUpdate(fechaUpdate);
				mercanciaFind.setNombreMercancia(nombreMercancia);
				mercanciaFind.setUsuarioIdUpdate(usuarioIdUpdate);				
				mercanciaRepository.save(mercanciaFind);
				log.log(Level.INFO, "Termina el metodo");
				return new Response(HttpStatus.OK.value(), "Registro Exitoso",mercanciaFind);
			}else {
				log.log(Level.INFO, "Error al actualizar en sistema");
				return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro mercancia en el sistema","{}");
			}						
		}else {
			log.log(Level.INFO, "Error al actualizar en sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro usuario en el sistema","{}");
		}		
	}

}
