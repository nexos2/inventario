package co.com.nexos.ws.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import co.com.nexos.ws.dto.CargoDTO;
import co.com.nexos.ws.entities.Cargo;
import co.com.nexos.ws.repository.CargoRepository;
/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Service
public class CargoService {
	
	@Autowired
	CargoRepository cargoRepository;
	
	private Logger log = Logger.getLogger("Log Cargo");
	
	public Cargo createCargo (CargoDTO cargoDTO) {
		log.log(Level.INFO, "Inicio Guardado cargo");
		Cargo cargo = new Cargo();		
		cargo.setNombreCargo(cargoDTO.getNombreCargo());
		cargo.setEstado(cargoDTO.getEstado());
		cargoRepository.save(cargo);
		log.log(Level.INFO, "Termina el metodo");		
		return cargo;
	}
	
	public CargoDTO findTaskByIdTarea(long idCargo) {		
		log.log(Level.INFO, "Inicio busqueda de cargo");
		Cargo cargoFind = cargoRepository.findByIdCargo(idCargo);	
		CargoDTO cargoDto = CargoDTO.builder()
					.idCargo(cargoFind.getIdCargo())
					.nombreCargo(cargoFind.getNombreCargo())
					.estado(cargoFind.getEstado())
					.build();
			log.log(Level.INFO, "Termina el metodo");
		return cargoDto;
	}	
	
	public List<CargoDTO> findCargoByNombreCargo(String nombreCargos) {
		log.log(Level.INFO, "Inicio busqueda de cargos");
		List<CargoDTO> cargosArray = new ArrayList<>();
		List<Cargo> listaCargos = cargoRepository.findCargoByNombreCargo(nombreCargos);
		
		for (Cargo cargoEach: listaCargos) {
			CargoDTO cargoDto = CargoDTO.builder()
				.idCargo(cargoEach.getIdCargo())
				.nombreCargo(cargoEach.getNombreCargo())
				.estado(cargoEach.getEstado())
				.build();
			cargosArray.add(cargoDto);
		}
		log.log(Level.INFO, "Termina Metodo");
		return cargosArray;
	}
	
	public List<CargoDTO> getAll() {
		log.log(Level.INFO, "Inicio listado de cargos");
		List<CargoDTO> cargoArray = new ArrayList<>();
		List<Cargo> listaCargos = cargoRepository.findAll();
		
		for (Cargo cargoEach: listaCargos) {			
			CargoDTO cargoDto = CargoDTO.builder()
					.idCargo(cargoEach.getIdCargo())
					.nombreCargo(cargoEach.getNombreCargo())
					.estado(cargoEach.getEstado())
					.build();
			cargoArray.add(cargoDto);
		}
		log.log(Level.INFO, "Termina el metodo");
		return cargoArray;
	}

	public CargoDTO updateCargo(long idCargo, String nombreCargo, String estado) {

		log.log(Level.INFO, "Inicio Actualizacion de cargo");		
		Cargo cargoUpdate = cargoRepository.findByIdCargo(idCargo);
		cargoUpdate.setNombreCargo(nombreCargo);
		cargoUpdate.setEstado(estado);	
		CargoDTO cargoDto = CargoDTO.builder()
				.idCargo(cargoUpdate.getIdCargo())
				.nombreCargo(cargoUpdate.getNombreCargo())
				.estado(cargoUpdate.getEstado())
				.estado(cargoUpdate.getEstado())
				.build();
		cargoRepository.save(cargoUpdate);
		log.log(Level.INFO, "Finaliza");

		return cargoDto;
	}
	
	public CargoDTO deleteCargo(long idCargo) {

		log.log(Level.INFO, "Inicio Borrado de cargo");
		Cargo cargoDelete = cargoRepository.findByIdCargo(idCargo);
		CargoDTO cargoDto = CargoDTO.builder()
				.idCargo(cargoDelete.getIdCargo())
				.nombreCargo(cargoDelete.getNombreCargo())
				.estado(cargoDelete.getEstado())
				.build();

		cargoRepository.delete(cargoDelete);

		log.log(Level.INFO, "Finalizacion del metodo");

		return cargoDto;
	}
}
