package co.com.nexos.ws.service;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import co.com.nexos.ws.dto.UsuarioDTO;
import co.com.nexos.ws.entities.Cargo;
import co.com.nexos.ws.entities.Usuario;
import co.com.nexos.ws.repository.CargoRepository;
import co.com.nexos.ws.repository.UsuarioRepository;
import co.com.nexos.ws.ui.model.response.Response;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository; 
	@Autowired
	CargoRepository cargoRepository;
	
	private Logger log = Logger.getLogger("Log Usuario");
	
	public Response createUsuario(UsuarioDTO usuarioDTO) {
		log.log(Level.INFO, "Inicio Guardado usuario");
		Usuario usuario = new Usuario();
		Cargo cargoEncontrado = cargoRepository.findByIdCargo(usuarioDTO.getIdCargo());
		if (cargoEncontrado != null) {	
			usuario.setUsuarioId(usuarioDTO.getUsuarioId());
			usuario.setNombre(usuarioDTO.getNombre());
			usuario.setEdad(usuarioDTO.getEdad());
			usuario.setCargo(cargoEncontrado);
			usuario.setFechaIngreso(usuarioDTO.getFechaIngreso());
			usuarioRepository.save(usuario);			
			log.log(Level.INFO, "Termina el metodo");
			return new Response(HttpStatus.OK.value(), "Registro Exitoso",usuario);
		}else {		
			log.log(Level.INFO, "Error al guardar en sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro cargo en el sistema","{}");
		}
	}
	
	public Response findUsuarioByIdUsuario(String idUSuario) {		
		log.log(Level.INFO, "Inicio busqueda de usuario");
		Usuario usuarioFind = usuarioRepository.findByUsuarioId(idUSuario);
		if(usuarioFind!=null) {
			Usuario usuario = new Usuario();
			Cargo cargo = new Cargo();
			cargo.setIdCargo(usuarioFind.getCargo().getIdCargo());
			cargo.setEstado(usuarioFind.getCargo().getEstado());
			cargo.setNombreCargo(usuarioFind.getCargo().getNombreCargo());
			usuario.setUsuarioId(usuarioFind.getUsuarioId());
			usuario.setNombre(usuarioFind.getNombre());
			usuario.setEdad(usuarioFind.getEdad());
			usuario.setCargo(cargo);
			usuario.setFechaIngreso(usuarioFind.getFechaIngreso());
			log.log(Level.INFO, "Termina el metodo");
			return new Response(HttpStatus.OK.value(), "Busqueda Exitosa",usuario);
		}else {
			log.log(Level.INFO, "No existe usuario en el sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro usuario en el sistema","{}");
		}		
	}
	
	public List<Usuario> findUsuarioByNombreUsuario(String nombre) {
		log.log(Level.INFO, "Inicio busqueda de usuarios");
		List<Usuario> usuarioArray = new ArrayList<>();
		List<Usuario> listaUsuario = usuarioRepository.findUsuarioByNombre(nombre);		
		for (Usuario usuariosEach: listaUsuario) {
			Usuario usuario = new Usuario();
			Cargo cargo = new Cargo();
			cargo.setIdCargo(usuariosEach.getCargo().getIdCargo());
			cargo.setEstado(usuariosEach.getCargo().getEstado());
			cargo.setNombreCargo(usuariosEach.getNombre());
			usuario.setUsuarioId(usuariosEach.getUsuarioId());
			usuario.setNombre(usuariosEach.getNombre());
			usuario.setEdad(usuariosEach.getEdad());
			usuario.setCargo(cargo);
			usuario.setFechaIngreso(usuariosEach.getFechaIngreso());
			usuarioArray.add(usuario);
		}
		log.log(Level.INFO, "Termina Metodo");
		return usuarioArray;
	}
	
	public List<Usuario> getAll() {
		log.log(Level.INFO, "Inicio listado de usuarios");
		List<Usuario> usuariosArray = new ArrayList<>();
		List<Usuario> listaUsuarios = usuarioRepository.findAll();
		
		for (Usuario usuarioEach: listaUsuarios) {			
			Usuario usuario = new Usuario();
			Cargo cargo = new Cargo();
			cargo.setIdCargo(usuarioEach.getCargo().getIdCargo());
			cargo.setNombreCargo(usuarioEach.getCargo().getNombreCargo());
			cargo.setEstado(usuarioEach.getCargo().getEstado());
			usuario.setUsuarioId(usuarioEach.getUsuarioId());
			usuario.setNombre(usuarioEach.getNombre());
			usuario.setEdad(usuarioEach.getEdad());
			usuario.setCargo(cargo);
			usuario.setFechaIngreso(usuarioEach.getFechaIngreso());
			usuariosArray.add(usuario);
		}
		log.log(Level.INFO, "Termina el metodo");
		return usuariosArray;
	}
	
	public Response updateUsuario(String usuarioId, String nombre, int edad, int cargo) {

		log.log(Level.INFO, "Inicio Actualizacion de usuario");		
		Usuario usuarioUpdate = usuarioRepository.findByUsuarioId(usuarioId);
		if(usuarioUpdate !=null) {
			Cargo cargoEncontrado = cargoRepository.findByIdCargo(cargo);			
			if(cargoEncontrado != null) {
				Cargo cargoNew = new Cargo();
				cargoNew.setIdCargo(cargoEncontrado.getIdCargo());
				cargoNew.setNombreCargo(cargoEncontrado.getNombreCargo());
				cargoNew.setEstado(cargoEncontrado.getEstado());
				usuarioUpdate.setNombre(nombre);
				usuarioUpdate.setEdad(edad);
				usuarioUpdate.setCargo(cargoNew);
				usuarioRepository.save(usuarioUpdate);
				log.log(Level.INFO, "Termina el metodo");
				return new Response(HttpStatus.OK.value(), "Registro Exitoso",usuarioUpdate);
			}else {
				log.log(Level.INFO, "Error al actualizar en sistema");
				return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro cargo en el sistema","{}");
			}						
		}else {
			log.log(Level.INFO, "Error al actualizar en sistema");
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro usuario en el sistema","{}");
		}		
	}
	
	public Response deleteUsuario(String usuarioId) {
		log.log(Level.INFO, "Inicio Borrado de usuario");
		Usuario usuarioDelete = usuarioRepository.findByUsuarioId(usuarioId);		
		if(usuarioDelete != null) {
			Cargo cargo = new Cargo();
			cargo.setIdCargo(usuarioDelete.getCargo().getIdCargo());
			cargo.setNombreCargo(usuarioDelete.getCargo().getNombreCargo());
			cargo.setEstado(usuarioDelete.getCargo().getEstado());
			usuarioDelete.setCargo(cargo);
			usuarioRepository.delete(usuarioDelete);
			log.log(Level.INFO, "Finalizacion del metodo");
			return new Response(HttpStatus.OK.value(), "eliminacion Exitosa",usuarioDelete);
		}else {
			return new Response(HttpStatus.NO_CONTENT.value(), "No se encontro usuario en el sistema","{}");
		}
	}

}
