package co.com.nexos.ws.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import co.com.nexos.ws.entities.Mercancia;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
public interface MercanciaRepository extends JpaRepository<Mercancia, Long> {
	
	Mercancia findByIdMercancia (long idMercancia);
	
	List<Mercancia> findMercanciaByNombreMercancia (String nombreMercancia);
}
