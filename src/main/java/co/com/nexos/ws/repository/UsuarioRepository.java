package co.com.nexos.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import co.com.nexos.ws.entities.Usuario;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	Usuario findByUsuarioId (String idUsuario);
	
	List<Usuario> findUsuarioByNombre (String nombre);
}
