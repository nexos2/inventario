package co.com.nexos.ws.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import co.com.nexos.ws.entities.Cargo;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
public interface CargoRepository extends JpaRepository<Cargo, Long> {
	
	Cargo findByIdCargo (long idCargo);
	
	List<Cargo> findCargoByNombreCargo (String nombreCargo);
}
