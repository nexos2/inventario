package co.com.nexos.ws.exception;

public class EntityServiceException extends RuntimeException {

	/**
	 *
	 * 
	 * @version 1.0.0
	 * @author Yoselin Cecilia Machado Vega
	 * @since 26/06/2022
	 * */
	private static final long serialVersionUID = 1L;
	
	public EntityServiceException(String message) {
		super(message);
	}
	

}
