package co.com.nexos.ws.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Getter
@Setter
@Data
@Entity
@Table(name= "usuario", schema = "public")
public class Usuario {
	
	@Id
	@NotEmpty(message = "El usuario_id no puede estar vacio o ser nulo")
	@Column(name="usuario_id")
	private String usuarioId;
	@NotEmpty(message = "El nombre no puede estar vacio o ser nulo")
	@Column(name="nombre")
	private String nombre;
	//@NotEmpty(message = "La edad no puede estar vacio o ser nulo")
	@Column(name="edad")
	private int edad;
	@Column(name="fecha_ingreso")
	private Date fechaIngreso;
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cargo", nullable = false)
	private Cargo cargo;

}
