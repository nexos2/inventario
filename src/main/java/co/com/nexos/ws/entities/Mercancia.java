package co.com.nexos.ws.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Getter
@Setter
@Data
@Entity
@Table(name= "mercancia", schema = "public")
public class Mercancia {
	
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mercancia")
    @SequenceGenerator(name = "seq_mercancia", sequenceName = "seq_mercancia", initialValue = 1, allocationSize = 1)	
	@Column(name = "id_mercancia")
	private long idMercancia;
	@NotEmpty(message = "El nombreMercancia no puede estar vacio o ser nulo")	
	@Column(name="nombre_mercancia")
	private String nombreMercancia;
	@Column(name="cantidad")
	private int cantidad;
	@Column(name="fecha_ingresa")
	private Date fechaIngreso;
	@Column(name="usuario_id_create")
	private String usuarioIdCreate;
	@Column(name="usuario_id_update")
	private String usuarioIdUpdate;
	@Column(name="fecha_update")
	private Date fechaUpdate;

}
