package co.com.nexos.ws.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Getter
@Setter
@Data
@Entity
@Table(name= "cargo", schema = "public")
public class Cargo {

	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cargo")
    @SequenceGenerator(name = "seq_cargo", sequenceName = "seq_cargo", initialValue = 1, allocationSize = 1)	
	@Column(name = "id_cargo")
	private long idCargo;
	@NotEmpty(message = "El nombre no puede estar vacio o ser nulo")
	@Column(name="nombre_cargo")
	private String nombreCargo;
	@NotEmpty(message = "El estado no puede estar vacio o ser nulo")
	@Column(name="estado")
	private String estado;	
	
}
