package co.com.nexos.ws.ui.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import co.com.nexos.ws.dto.CargoDTO;
import co.com.nexos.ws.dto.MercanciaDTO;
import co.com.nexos.ws.dto.UsuarioDTO;
import co.com.nexos.ws.entities.Mercancia;
import co.com.nexos.ws.entities.Usuario;
import co.com.nexos.ws.exception.EntityServiceException;
import co.com.nexos.ws.service.CargoService;
import co.com.nexos.ws.service.MercanciaService;
import co.com.nexos.ws.service.UsuarioService;
import co.com.nexos.ws.ui.model.response.Response;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@RequestMapping(path = "${controller.properties.base-path}") 
@RestController
public class GestionController {
	
	@Autowired
	private CargoService serviceCargo;
	@Autowired
	private UsuarioService serviceUsuario;
	@Autowired
	private MercanciaService serviceMercancia;
			
	@PostMapping(value = "/createCargo")
    public Response createCargo (@Valid @RequestBody(required= true) CargoDTO cargoDTO) {
		try {
			serviceCargo.createCargo(cargoDTO);
			return new Response(HttpStatus.OK.value(), "Registro Exitoso", "Guardado Exitoso");
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Guardar el registro", "Error!");
		}
	}
	
	@GetMapping(value = "/findByCargoId")
	public Response findByIdCargo(@RequestParam long idCargo) {
		try {
			CargoDTO cargoFind = serviceCargo.findTaskByIdTarea(idCargo);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", cargoFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	
	@GetMapping(value = "/findCargoByNombreCargo")
	public Response findCArgoByNombres(@RequestParam String nombreCargo) {
		try {
			List<CargoDTO> cargoDTOFind = serviceCargo.findCargoByNombreCargo(nombreCargo);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", cargoDTOFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	
	@GetMapping(value = "/getAllcargo")
	public Response getAllCargo() {
			List<CargoDTO> listaCargo = serviceCargo.getAll();
			ArrayList<Object> reponseAllConv = new ArrayList<>();
			if (listaCargo!=null && listaCargo.isEmpty())
			{
				return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), reponseAllConv);
			}
			else {
				return new Response(HttpStatus.OK.value(), "Lista Total Exitosa", listaCargo);
			}		
	}
	
	@PostMapping(value = "/updateCargo")
	public Response updateCargo(@RequestParam long idCargo, @RequestParam String nombreCompleto, @RequestParam String estado) {
		try {
			CargoDTO cargoUpdate = serviceCargo.updateCargo(idCargo, nombreCompleto, estado);
			return new Response(HttpStatus.OK.value(), "Actualizacion Exitoso", cargoUpdate);
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo actualizar el registro", "Error en la Solicitud");
		}
	}
	
	@PostMapping(value = "/deleteCargo")
	public Response deleteCargo(@RequestParam long idCargo) {
		try {
			CargoDTO cargoDelete = serviceCargo.deleteCargo(idCargo);
			return new Response(HttpStatus.OK.value(), "Eliminado Exitoso",cargoDelete);
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Eliminar el registro", "Error en la Peticion");
		}
	}
	
	@PostMapping(value = "/createUsuario")
    public Response createUsuario (@Valid @RequestBody(required= true) UsuarioDTO usuarioDTO) {
		try {
			Response respuesta = serviceUsuario.createUsuario(usuarioDTO);
			return respuesta;
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Guardar el registro", "Error!");
		}
	}
	
	@GetMapping(value = "/findByIdUsuario")
	public Response findByIdUsuario(@RequestParam String idUsuario) {
		try {
			Response respuesta = serviceUsuario.findUsuarioByIdUsuario(idUsuario);
			return respuesta;	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	
	@GetMapping(value = "/findUsuarioByNombre")
	public Response findUsuarioByNombres(@RequestParam String nombre) {
		try {
			List<Usuario> UsuarioFind = serviceUsuario.findUsuarioByNombreUsuario(nombre);
			return new Response(HttpStatus.OK.value(), "Listado Exitoso", UsuarioFind);	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	
	@GetMapping(value = "/getAllusuarios")
	public Response getAllUsuario() {
			List<Usuario> listaUsuario = serviceUsuario.getAll();
			ArrayList<Object> reponseAllConv = new ArrayList<>();
			if (listaUsuario!=null && listaUsuario.isEmpty())
			{
				return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), reponseAllConv);
			}
			else {
				return new Response(HttpStatus.OK.value(), "Lista Total Exitosa", listaUsuario);
			}		
	}
	
	@PostMapping(value = "/updateUsuario")
	public Response updateUsuario(@RequestParam String usuarioId, @RequestParam String nombre, @RequestParam int edad,  @RequestParam int cargo) {
		try {
			Response respuesta = serviceUsuario.updateUsuario(usuarioId, nombre, edad, cargo);
			return respuesta;
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo actualizar el registro", "Error en la Solicitud");
		}
	}
	
	@PostMapping(value = "/deleteUsuario")
	public Response deleteUsuario(@RequestParam String usuarioId) {
		try {
			Response respuesta = serviceUsuario.deleteUsuario(usuarioId);
			return respuesta;
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Eliminar el registro", "Error en la Peticion");
		}
	}
	
	@PostMapping(value = "/createMercancia")
    public Response createMercancia (@Valid @RequestBody(required= true) MercanciaDTO mercanciaDTO) {
		try {
			Response respuesta = serviceMercancia.createMaercancia(mercanciaDTO);
			return respuesta;
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo Guardar el registro", "Error!");
		}
	}
	
	@GetMapping(value = "/findByIdMercancia")
	public Response findByIdMercancia(@RequestParam Long idMercancia) {
		try {
			Response respuesta = serviceMercancia.findMercanciaByIdMercancia(idMercancia);
			return respuesta;	
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), "No hay Datos con el parametro enviado");
		}	
	}
	
	@GetMapping(value = "/getAllmercancias")
	public Response getAllMercancias() {
		List<Mercancia> listaMercancia = serviceMercancia.getAll();
		ArrayList<Object> reponseAllConv = new ArrayList<>();
		if (listaMercancia != null && listaMercancia.isEmpty()) {
			return new Response(HttpStatus.NO_CONTENT.value(), HttpStatus.BAD_REQUEST.name(), reponseAllConv);
		} else {
			return new Response(HttpStatus.OK.value(), "Lista Total Exitosa", listaMercancia);
		}
	}
	
	@PostMapping(value = "/updateMercancia")
	public Response updatemercancia(@RequestParam long idMercancia, @RequestParam int cantidad,  @RequestParam String fechaUpdate,  @RequestParam String nombreMercancia,  @RequestParam String usuarioIdUpdate) {
		try {
			Date fecha = null;
			try {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			fecha = formato.parse(fechaUpdate);
			}catch (ParseException e) {				
				e.printStackTrace();
			}
			Response respuesta = serviceMercancia.updateMercancia(idMercancia, cantidad, fecha, nombreMercancia, usuarioIdUpdate);
			return respuesta;
		} catch (EntityServiceException exception) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "No se pudo actualizar el registro", "Error en la Solicitud");
		} 
	}
}
