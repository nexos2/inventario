package co.com.nexos.ws.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
*
* 
* @version 1.0.0
* @author Yoselin Cecilia Machado Vega
* @since 27/06/2022
* */
@Builder
@Setter
@Getter
@AllArgsConstructor()
public class MercanciaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	private long idMercancia;
	private String nombre;
	private int cantidad;
	private Date fechaIngresa;
	private String usuarioIdCreate;
	private String usuarioIdUpdate;
	private Date fechaUpdate;
	
	public MercanciaDTO() {}

}
